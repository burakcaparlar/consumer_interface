from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User


class Hotel(models.Model):
    name = models.CharField(max_length=100)
    coral_code = models.CharField(max_length=100, unique=True)


class Destination(models.Model):
    name = models.CharField(max_length=100)
    coral_code = models.CharField(max_length=100, unique=True)


class Booking(models.Model):
    status_choices = (('succeeded', 'SUCCEEDED'), ('FALSE', 'false'))

    user = models.ForeignKey(User)
    provision_code = models.CharField(max_length=100)
    hotel = models.ForeignKey(Hotel)
    booking_code = models.CharField(max_length=40, unique=True)
    coral_booking_code = models.CharField(max_length=40)
    room_type = models.CharField(max_length=100)
    room_description = models.CharField(max_length=100)
    pax_count = models.IntegerField(default=1)
    pax_names = models.CharField(max_length=200)
    status = models.CharField(max_length=9,
                              choices=status_choices)
    price = models.FloatField()

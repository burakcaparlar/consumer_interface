from django.core.management.base import BaseCommand
import csv

from client.models import Hotel
from client.models import Destination


class Command(BaseCommand):
    can_import_settings = True

    def add_arguments(self, parser):
        parser.add_argument('load_file')
        parser.add_argument('file_name')

    def handle(self, *args, **options):
        with open(options["load_file"], 'rb') as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
            for hotels in csv_reader:
                hotel_code = hotels[0]
                hotel_name = hotels[1]

                if options['file_name'] == 'hotel':

                    db_model = Hotel

                elif options['file_name'] == 'destination':

                    db_model = Destination

                else:
                    raise ValueError('No Longer Available')  # Expected

                if not db_model.objects.get(coral_code=hotel_code):
                    db = db_model.objects.create(
                        name=hotel_name,
                        coral_code=hotel_code
                    )
                    db.save()

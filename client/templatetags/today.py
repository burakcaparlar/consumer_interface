from django import template
import datetime


def today():
    return datetime.date.today()


register = template.Library()
register.filter('today', today)

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^detail/$', views.detail, name='detail'),
    url(r'^hotel_search/(?P<hotel_code>[\w-]+)', views.hotel_search,
        name='hotel_search'),
    url(r'^info/(?P<prod_code>[\w-]+)', views.info,
        name='info'),
    url(r'^hotels/$', views.hotel_search, name='hotel_search'),
    url(r'^booking/$', views.booking, name='booking'),
    url(r'^my_bookings/$', views.my_bookings, name='my_bookings'),

]

from django.contrib.auth.decorators import login_required
from booker import Booker
from django.shortcuts import render
from client.models import Hotel
from client.models import Destination, Booking
from datetime import timedelta, datetime, date
from django.template.loader import render_to_string
from django.utils import crypto
from django.core.mail import EmailMessage
from consumer_interface import local_settings as ls
from django.contrib import messages
from django.shortcuts import redirect
import redis
import hashlib
import json
import concurrent.futures as cf

book = Booker(password=ls.API_PASSWORD, username=ls.API_USERNAME)
search_cache = redis.StrictRedis(host='localhost', port=6379)


@login_required
def index(request):
    destinations = Destination.objects.all()
    today = date.today().strftime('%Y-%m-%d')
    tomorrow = date.today() + timedelta(days=1)
    return render(request, 'client/index.html', {'destinations': destinations,
                                                 'today': today,
                                                 'tomorrow': tomorrow.strftime(
                                                     '%Y-%m-%d')})


@login_required
def detail(request):
    request.session['pax'] = request.GET.get('pax')
    destination_code = request.GET.get('destination')
    request.session['checkin'] = request.GET.get('checkin')
    request.session['checkout'] = request.GET.get('checkout')

    params = param(request, request.session['checkin'],
                   request.session['checkout'],
                   request.session['pax'],
                   destination_code=destination_code)

    response = cache(params=params)

    hotel_codes_db = Hotel.objects.values_list('coral_code')
    hotel_codes_db = [codes[0] for codes in hotel_codes_db]

    hotel_data = []
    for hotel in response["results"]:
        if hotel["hotel_code"] in hotel_codes_db:
            hotel_data.append({
                "hotel": Hotel.objects.get(coral_code=hotel["hotel_code"]),
                "price": min_price(hotel),
                "hotel_code": hotel["hotel_code"]

            })

    return render(request, 'client/detail.html', {'hotel_data': hotel_data})


def min_price(hotel):
    return min(hotel["products"], key=lambda k: float(k['price']))['price']


@login_required()
def hotel_search(request, hotel_code):
    request.session['hotel_code'] = hotel_code
    params = param(request, request.session['checkin'],
                   request.session['checkout'],
                   request.session['pax'],
                   hotel_code=hotel_code)

    response = cache(params=params)

    room_data = sorted(response["results"][0]["products"],
                       key=lambda a: float(a["price"]))

    return render(request, 'client/hotels.html',
                  {'room_data': room_data})


@login_required()
def info(request, prod_code):
    availability_response = cache(prod_code=prod_code)[0]
    resp = cache(prod_code=prod_code)[1]
    room_data = []
    if resp == 200:
        room_data.append({
            "pax_range": xrange(int(request.session['pax'])),
            "hotel_name": Hotel.objects.get(
                coral_code=availability_response["hotel_code"]).name,
            "prod_code": prod_code,
            "per_night": calculate_per_night(availability_response)
        })
        return render(request, 'client/info.html',
                      {'availability_response': resp,
                       'room_data': room_data,
                       'result': availability_response})
    else:
        messages.add_message(request, messages.INFO,
                             'Sectiginiz oda alinmis /'
                             'lutfen baska bir oda seciniz')
        return redirect('hotel_search',
                        hotel_code=request.session['hotel_code'])


@login_required()
def booking(request):
    name_params = []
    for name in request.GET.getlist('pax_name'):
        name_list = name.strip().split()
        lname = name_list[-1]
        fname = ' '.join(name_list[:-1])
        if not lname or not fname:
            messages.add_message(request, messages.INFO,
                                 'Isminizi duzgun giriniz')
            return redirect('info', prod_code=request.GET.get('prod_code'))
        else:
            name_params.append('1,{},{},adult'.format(fname, lname))
            provision_response = book.provision(
                product_code=request.GET.get('prod_code'))
            try:
                book_response = book.book(provision_response[0]["code"],
                                          {"name": name_params})
            except:
                messages.add_message(request, messages.INFO,
                                     'Sectiginiz oda alinmis/'
                                     'lutfen baska bir oda seciniz')
                return redirect('hotel_search',
                                hotel_code=request.session['hotel_code'])

            if book_response[1] == 200:
                room = book_response[0]["confirmation_numbers"][0]["rooms"][0]
                with cf.ThreadPoolExecutor(max_workers=1) as executor:
                    executor.submit(send_mail, message(request, book_response),
                                    request.GET.get('email'))
                confirmation = book_response[0]["confirmation_numbers"][0]
                booking = Booking.objects.create(
                    user=request.user,
                    provision_code=provision_response[0]["code"],
                    hotel=Hotel.objects.get(
                        coral_code=book_response[0]["hotel_code"]),
                    coral_booking_code=book_response[0]["code"],
                    room_type=room["room_type"],
                    room_description=room["room_description"],
                    pax_count=len(request.GET.getlist('pax_name')),
                    pax_names=confirmation["names"][0],
                    status=book_response[0]["status"],
                    price=float(book_response[0]["price"]),
                    booking_code=crypto.get_random_string(length=12)
                )

                booking.save()
                return render(request, 'client/booking.html',
                              {'book_response': book_response[1]})
            else:
                messages.add_message(request, messages.INFO,
                                     'Book sirasinda bir hata meydana geldi')
                return redirect('info', prod_code=request.GET.get('prod_code'))


@login_required()
def my_bookings(request):
    return render(request, 'client/my_bookings.html',
                  {'bookings': Booking.objects.all()})


def calculate_per_night(availability_response):
    return float(
        availability_response["price"]) / float(
        (datetime.strptime(availability_response["checkout"],
                           '%Y-%m-%d') - datetime.strptime(
            availability_response["checkin"], '%Y-%m-%d')).days)


def param(request, checkin, checkout, pax, hotel_code=None,
          destination_code=None):
    search_type = "destination_code" if destination_code else "hotel_code"
    search_code = destination_code if destination_code else hotel_code

    params = {'checkin': str(checkin), 'checkout': str(checkout),
              'pax': str(pax),
              search_type: search_code,
              'client_nationality': 'tr',
              'currency': 'USD', 'name': '1,test,test,adult'}

    return params


def cache(params=None, prod_code=None):
    if prod_code:
        hash = prod_code
    else:
        hash = hashlib.md5(
            json.dumps(params, sort_keys=True)
        ).hexdigest()

    resp = search_cache.get(hash)

    if resp:
        return json.loads(resp)
    elif prod_code:
        resp = book.availability(prod_code)
        search_cache.set(hash, json.dumps(resp),
                         ex=3600)
    else:
        resp = book.search(params)
        search_cache.set(hash, json.dumps(resp),
                         ex=3600)

    return resp


def send_mail(BODY, to):
    """With this function we send out our html email"""

    subject, from_email, to = 'Booking Info', 'metglobaltest@gmail.com', to

    email = EmailMessage(subject=subject, body=BODY, from_email=from_email,
                         to=[to])
    email.content_subtype = 'html'
    email.send()


def message(request, book_response):
    return render_to_string('client/mail.html',
                            {'book_response': book_response[0],
                             'hotel_name': Hotel.objects.get(
                                 coral_code=book_response[0][
                                     "hotel_code"]).name})
